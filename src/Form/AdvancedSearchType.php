<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\FileSubmission;
use App\Entity\User;
use App\Type\FileSubmissionState;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class AdvancedSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('query', SearchType::class,
                [
                    'attr'=> [
                        'placeholder' => 'search.query'
                    ]
                ]
            )
            ->add('category', EntityType::class,
                [
                    'class' => Category::class,
                    'label' => 'search.category',
                    'required' => false,
                    'placeholder' => 'search.allCategories',
                    'group_by' => function(Category $category, $key, $value) {
                        // value: Category's ID
                        // key: Category's index
                        while ($category->getParent() && $category->getParent()->getId() != $category->getId()) {
                            $category = $category->getParent();
                        }
                        return $category->getName();
                    }
                ]
            )
            ->add('verifiedOnly', CheckboxType::class,
                [
                    'label' => 'search.verifiedOnly',
                    'required' => false
                ]
            )
            ->add('save', SubmitType::class,
                [
                    'label' => 'search.submitButton'
                ]
            )
        ;
    }
}