<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\FileSubmission;
use App\Entity\User;
use App\Type\FileSubmissionState;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class FileSubmissionType extends AbstractType
{
    protected $is_admin;

    public function __construct(TokenStorageInterface $ts)
    {
        $user = $ts->getToken()->getUser();
        if ($user instanceof User)
            $this->is_admin = $user->hasRole('ROLE_ADMIN');
        else
            $this->is_admin = false;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'submission.title'
                ]
            )
            ->add('hash', TextType::class,
                [
                    'label' => 'submission.hash'
                ]
            )
            ->add('summary', TextType::class,
                [
                    'label' => 'submission.summary',
                    'required' => false
                ]
            )
            ->add('description', TextareaType::class,
                [
                    'label' => 'submission.description',
                    'required' => false
                ]
            )
            ->add('category', EntityType::class,
                [
                    'class' => Category::class,
                    'label' => 'submission.category',
                    'group_by' => function(Category $category, $key, $value) {
                        // value: Category's ID
                        // key: Category's index
                        while ($category->getParent() && $category->getParent()->getId() != $category->getId()) {
                            $category = $category->getParent();
                        }
                        return $category->getName();
                    }
                ]
            )
        ;

        if ($this->is_admin) {
            // Admins cannot delete post, only ban them
            $availableStates = FileSubmissionState::getReadableAvailableValues();
            if (($key = array_search('deleted', $availableStates)) !== false) {
                unset($availableStates[$key]);
            }

            $builder->add('state', ChoiceType::class,
                [
                    'choices' => $availableStates
                ]
            );
        }

        $builder->add('save', SubmitType::class, ['label' => 'submission.submitButton']);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FileSubmission::class,
        ]);
    }
}