<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserReportFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('data', TextType::class,
                [
                    'label' => 'report.reportReason',
                    'help' => 'report.reportReasonHelp',
                    'required' => false
                ]
            )
            ->add('save', SubmitType::class,
                [
                    'label' => 'report.submitReport'
                ]
            )
        ;
    }
}