<?php

namespace App\EventListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();
        $token = $data['token'];
        $event->getResponse()->headers->setCookie(
            new Cookie(
                'BEARER',                 // cookie name, should be the same as in JWT settings
                $token,                          // the cookie value, e.g. the generated JWT token
                new \DateTime('+1 hour'),  // the expiration
                null,                       // the path
                null,                    // the domain, null means that Symfony will generate it on its own
                false,                     // secure, e.g. only via https
                true,                   // http only cookie, which is the default so no need to specify
                false,                      // raw
                null                    // the same-site parameter, can be 'lax' or 'strict'
            )
        );

        if (!$user instanceof UserInterface) {
            return;
        }

        $event->setData(['roles' => $user->getRoles()]);
    }
}
