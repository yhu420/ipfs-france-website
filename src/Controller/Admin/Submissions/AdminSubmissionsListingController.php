<?php

namespace App\Controller\Admin\Submissions;

use App\Entity\FileSubmission;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminSubmissionsListingController extends Controller
{
    /**
     * @Route("/admin/submissions/listing", name="admin_submissions_listing")
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EntityManagerInterface $em)
    {
        $submissions = $em->getRepository(FileSubmission::class)->findAll();
        return $this->render('admin/submissions/listing.html.twig', ['submissions' => $submissions]);
    }
}
