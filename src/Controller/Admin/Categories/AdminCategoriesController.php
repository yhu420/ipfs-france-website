<?php

namespace App\Controller\Admin\Categories;

use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminCategoriesController extends Controller
{
    /**
     * @Route("/admin/categories", name="admin_edit_categories")
     * @param Request $request
     * @param FormFactoryInterface $ff
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, FormFactoryInterface $ff, EntityManagerInterface $em)
    {
        $all_cats = $em->getRepository(Category::class)->findAll();
        $new_cat = new Category();
        $new_cat->setName('Create a new category');
        $new_cat->setId(-1); // Doing this for the named forms, making sure it has a unique ID
        $all_cats[] = $new_cat;

        $form_views = [];
        foreach ($all_cats as $cat) {
            // Named forms to make sure to edit one entity at a time
            $form = $ff->createNamed('cat_form_' . $cat->getId(),CategoryType::class, $cat);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $cat = $form->getData();
                $em->persist($cat);
                $em->flush();

                // Doing this, otherwise form views are lagging one submit behind
                return $this->redirect($request->getUri());
            }

            $form_views[] = $form->createView();
        }

        return $this->render('admin/categories/manage.html.twig', ['cat_forms' => $form_views]);
    }
}
