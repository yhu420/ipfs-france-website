<?php

namespace App\Controller\Admin\Landing;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminLandingController extends Controller
{
    /**
     * @Route("/admin/", name="admin_landing")
     */
    public function index()
    {
        return $this->render('admin/landing/index.html.twig');
    }
}
