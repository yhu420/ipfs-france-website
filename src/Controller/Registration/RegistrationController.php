<?php

namespace App\Controller\Registration;

use App\Service\FormErrorFormatter;
use App\Service\UserAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class RegistrationController extends Controller
{
    /**
     * @Route("{_locale}/register", name="user_registration", requirements={"_locale" = "en|fr"})
     * @param Request $request
     * @param UserAPI $user_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerFromForm(Request $request, UserAPI $user_api)
    {
        $api_return = $user_api->registerNewUser($request->request->all());
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, user created
                return $this->redirectToRoute('main_page');
                break;

            case 1:
            case 2:
                // Form is not valid, or not submitted
                $form = $api_return['form'];
                return $this->render(
                    'registration/register.html.twig',
                    [
                        'form' => $form->createView()
                    ]
                );
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/register", name="user_registration_api")
     * @param Request $request
     * @param UserAPI $user_api
     * @Method("POST")
     * @return Response
     */
    public function registerFromAPI(Request $request, UserAPI $user_api) {
        $arr = $request->getContent();
        $arr = json_decode($arr, true);
        $api_return = $user_api->registerNewUser($arr, true);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, user created
                return new JsonResponse(['success' => true]);
                break;

            case 1:
            case 2:
                // Form is not valid
                // TODO: see if I can get the invalid fields
                return new JsonResponse(
                    [
                        'success' => false,
                        'error' => $api_return['errcode'],
                        'debug' => FormErrorFormatter::getErrorsAsArray($api_return['form'])
                    ]);
                break;

            default:
                return new Response('What?', 500);
        }
    }
}