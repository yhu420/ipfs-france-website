<?php

namespace App\Controller\Registration;

use App\Service\UserAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class GoogleRegistrationController extends Controller
{
    /**
     * @Route("{_locale}/register/google-callback", name="user_register_google_callback", requirements={"_locale" = "en|fr"})
     * @Method("POST")
     * @param Request $request
     * @param UserAPI $user_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerFromForm(Request $request, UserAPI $user_api)
    {
        $api_return = $user_api->registerNewUserGoogle($request->request->all());
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, user created
                return $this->redirectToRoute('main_page');
                break;

            case 1: // Non specified username
            case 2: // Incorrect username length
            case 3: // JWT token from Google not found
            case 4: // Failed to verify Google's JWT token
            case 5: // Duplicate email or username
                return $this->render(
                    'registration/register.html.twig',
                    [
                        'error' => 'Error: code ' . $errcode,
                        'form' => $api_return['form']->createView()
                    ]
                );
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/register/with-google", name="user_registration_google_api")
     * @param Request $request
     * @param UserAPI $user_api
     * @Method("POST")
     * @return Response
     */
    public function registerFromAPI(Request $request, UserAPI $user_api) {
        try {
            $all = json_decode($request->getContent(), true);
        } catch (\Exception $e) {
            return new Response('Could not parse JSON', 500);
        }
        $api_return = $user_api->registerNewUserGoogle($all);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, user created
                return new JsonResponse([
                    'success' => true
                ]);
                break;

            case 1: // Non specified username
            case 2: // Incorrect username length
            case 3: // JWT token from Google not found
            case 4: // Failed to verify Google's JWT token
            case 5: // Duplicate email or username
                return new JsonResponse(
                    [
                        'success' => false,
                        'error' => $errcode
                    ]
                );
                break;

            default:
                return new Response('What?', 500);
        }
    }
}