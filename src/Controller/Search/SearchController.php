<?php

namespace App\Controller\Search;

use App\Service\SearchAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SearchController extends Controller
{
    /**
     * @Route("{_locale}/search", name="search_results", requirements={"_locale" = "en|fr"})
     * @Method("GET")
     * @param Request $request
     * @param SearchAPI $search_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function getSearchResults(Request $request, SearchAPI $search_api)
    {
        $api_return = $search_api->getSearchResults($request->query->all());
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return $this->render('search_results/search_results.html.twig', $api_return);
                break;

            case 1:
                // Form is not valid, or not submitted
                return $this->redirectToRoute('main_page');
                break;

            default:
                return new Response('What?', 500);
        }
    }


    /**
     * @Route("/api/v1/submission/search", name="user_search_submission_api")
     * @Method("POST")
     */
    public function searchFromAPI(Request $request, SearchAPI $search_api) {
        $all = json_decode($request->getContent(), true);
        $api_return = $search_api->getSearchResults($all);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return new JsonResponse(['success' => true, 'results' => $api_return['results']]);
                break;

            default:
                return new JsonResponse(['success' => false, 'error' => 1]);
        }
    }

}