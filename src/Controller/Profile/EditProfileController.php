<?php

namespace App\Controller\Profile;

use App\Service\UserAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class EditProfileController extends Controller
{
    /**
     * @Route("{_locale}/profile/edit/{username}", name="user_edit_profile", requirements={"_locale" = "en|fr"})
     * @param Request $request
     * @param $username
     * @param UserAPI $user_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editProfileFromSite(Request $request, $username, UserAPI $user_api)
    {
        $api_return = $user_api->editUserProfile($request->request->all(), $username);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0: // Form submitted
                return $this->redirectToRoute('user_consult_profile', ['username' => $username]);
                break;
            case 3: // Form not submitted
            case 4: // Form submitted but invalid data
                return $this->render('profile/edit.html.twig', ['form' => $api_return['form']]);
                break;

            case 1:
                // User not found
                throw $this->createNotFoundException('Utilisateur introuvable!');
                break;

            case 2:
                // Insufficient privileges
                throw $this->createAccessDeniedException('Only the owner of the profile can edit it');
                break;


            default:
                return new Response('What?', 500);
        }
    }

    /**
     * Never fails: user has to be login because of firewall. Only persists if valid data.
     *
     * @Route("/api/v1/profile/editmine", name="user_edit_my_profile_api")
     * @Method("POST")
     * @param Request $request
     * @param UserAPI $user_api
     * @return Response
     */
    public function editProfileFromApi(Request $request, UserAPI $user_api) {
	$assoc = json_decode($request->getContent(), true);
        $user_api->editMyProfileAPI($assoc);
        return $this->json(['success' => true]);
    }

}
