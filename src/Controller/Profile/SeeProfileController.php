<?php

namespace App\Controller\Profile;

use App\Service\UserAPI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SeeProfileController extends Controller
{
    /**
     * @Route("{_locale}/profile/see/{username}", name="user_consult_profile", requirements={"_locale" = "en|fr"})
     * @param $username
     * @param UserAPI $user_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function seeProfileFromSite($username, UserAPI $user_api)
    {
        $api_return = $user_api->seeUserProfile($username);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return $this->render('profile/see.html.twig', ['user' => $api_return['user']]);
                break;

            case 1:
                // User not found
                throw $this->createNotFoundException('Utilisateur introuvable!');
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/profile/read/{username}", name="user_read_profile_api")
     * @Method("POST")
     * @param $username
     * @param UserAPI $user_api
     * @return Response
     */
    public function seeProfileFromApi($username, UserAPI $user_api) {
        $api_return = $user_api->seeUserProfile($username);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $u = $api_return['user'];
                return new JsonResponse(
                    [
                        'success' => true,
                        'user' => $u->toArray(),
                        'submissions' => array_map(
                            function ($s) {return $s->toArray(false);},
                            $u->getFileSubmissions()->toArray()
                        )
                    ]
                );
                break;

            case 1:
                // User not found
                return new JsonResponse([
                    'success' => false,
                    'error' => 1
                ], 404);
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/profile/readmine", name="user_readmine_profile_api")
     * @Method("POST")
     * @param UserAPI $user_api
     * @return Response
     */
    public function seeMyProfileFromApi(UserAPI $user_api) {
        $api_return = $user_api->seeMyUserProfile();
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $u = $api_return['user'];
                return new JsonResponse(
                    [
                        'success' => true,
                        'user' => $u->toArray()
                    ]
                );
                break;

            case 1:
                // Not logged in
                return new JsonResponse([
                    'success' => false,
                    'error' => 1,
                    'debug' => $api_return['debug']
                ], 401);
                break;

            default:
                return new Response('What?', 500);
        }
    }

}