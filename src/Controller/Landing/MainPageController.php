<?php

namespace App\Controller\Landing;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainPageController extends Controller
{
    /**
     * @Route("/{_locale}", name="main_page", requirements={"_locale" = "en|fr"})
     */
    public function index()
    {
        return $this->render('landing/landing.html.twig');
    }

    /**
     * @Route("/", name="go_to_main_page")
     */
    public function redirectToMainSite(Request $request)
    {
        $answer = $request->headers->get('no-answer', null);
        if (is_null($answer))
            return $this->redirectToRoute('main_page');
        else
            return new JsonResponse(['success' => true]);
    }
}
