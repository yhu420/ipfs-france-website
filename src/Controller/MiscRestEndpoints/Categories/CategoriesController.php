<?php

namespace App\Controller\MiscRestEndpoints\Categories;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CategoriesController extends Controller
{
    /**
     * @Route("/api/v1/category/read", name="user_read_categories_api")
     * @Method("POST")
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function seeProfileFromApi(EntityManagerInterface $em) {
        $categories = $em->getRepository(Category::class)->findAll();
        $categories = array_map(
            function ($cat) {
                return $cat->toArray();
            },
            $categories);

        return $this->json(
            [
                'success' => true,
                'categories' => $categories
            ]
        );
    }

}