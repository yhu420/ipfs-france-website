<?php

namespace App\Controller\MiscRestEndpoints\Reactions;

use App\Entity\FileSubmission;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ReactionsController extends Controller
{
    /**
     * @Route("/api/v1/reaction/request/mine/{submission_id}", name="user_request_own_reactions_api")
     * @Method("POST")
     * @param $submission_id
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function getMyReactionsForFileSubmissionFromApi($submission_id, EntityManagerInterface $em) {
        $submission = $em->getRepository(FileSubmission::class)->find($submission_id);
        if (is_null($submission))
            return new JsonResponse(['success' => false, 'error' => 1], 404);

        $report = $this->getUser()->getReport($submission);
        $r = [
            'success' => true,
            'upvote' => (boolean)$this->getUser()->getUpvote($submission),
            'downvote' => (boolean)$this->getUser()->getDownvote($submission),
            'report' => is_null($report) ? false : $report->getData()
        ];

        return new JsonResponse($r);
    }

}