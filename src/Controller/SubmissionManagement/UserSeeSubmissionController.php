<?php

namespace App\Controller\SubmissionManagement;

use App\Service\SubmissionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserSeeSubmissionController extends Controller
{
    /**
     * @Route("/{_locale}/submission/see/{submission_id}/{slug}", name="user_see_submission", requirements={"_locale" = "en|fr"}, defaults={"slug": "doitforme"})
     * @param $submission_id
     * @param $slug
     * @param SubmissionAPI $submission_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function seeFromSite($submission_id, $slug, SubmissionAPI $submission_api)
    {
        $api_return = $submission_api->seeSubmission($submission_id, $slug);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return $this->render('submission_management/user_see.html.twig', $api_return);
                break;

            case 1: // Not found
                throw $this->createNotFoundException('Unable to find the requested submission');
            case 2: // Banned
                throw $this->createAccessDeniedException('This submission contained sensible data and is not available anymore');
            case 3: // Wrong slug
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission_id,
                        'slug' => $api_return['correct_slug']
                    ]
                );
            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/submission/read/{submission_id}", name="user_see_submission_api")
     * @Method("POST")
     * @param $submission_id
     * @param SubmissionAPI $submission_api
     * @return JsonResponse
     */
    public function seeFromApi($submission_id, SubmissionAPI $submission_api)
    {
        $api_return = $submission_api->seeSubmission($submission_id, null);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                    return new JsonResponse([
                        'success' => true,
                        'submission' => $api_return['submission']->toArray()
                    ]);
                break;

            case 1: // Not found
                return new JsonResponse(['success' => false, 'error' => 1], 404);
            case 2: // Banned
                return new JsonResponse(['success' => false, 'error' => 2], 403);
            default:
                return new JsonResponse(['success' => false, 'error' => 99], 500);
        }
    }
}
