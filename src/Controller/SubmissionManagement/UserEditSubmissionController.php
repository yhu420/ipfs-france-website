<?php

namespace App\Controller\SubmissionManagement;

use App\Service\FormErrorFormatter;
use App\Service\SubmissionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserEditSubmissionController extends Controller
{
    /**
     * @Route("/{_locale}/submission/edit/{submission_id}/", name="user_edit_submission", requirements={"_locale" = "en|fr"})
     *
     * @param Request $request
     * @param SubmissionAPI $submission_api
     * @param $submission_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editFromSite(Request $request, SubmissionAPI $submission_api, $submission_id)
    {
        $api_return = $submission_api->editFile($request->request->all(), $submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, edition successful
                $submission = $api_return['submission'];
                return $this->redirectToRoute('user_see_submission', ['submission_id' => $submission->getId()] );
                break;

            case 1: // Submission not found
            case 2: // Submission doesn't belong to the current user
                return $this->render('errors/not_allowed.html.twig');
            case 3:
            case 4:
                // Form is not valid, or not submitted
                $form = $api_return['form'];
                return $this->render(
                    'submission_management/user_edit.html.twig',
                    [
                        'form' => $form->createView()
                    ]
                );
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/submission/edit/{submission_id}", name="user_edit_submission_api")
     * @Method("POST")
     * @param Request $request
     * @param SubmissionAPI $submission_api
     * @param $submission_id
     * @return JsonResponse
     */
    public function editFromAPI(Request $request, SubmissionAPI $submission_api, $submission_id)
    {
        $all = json_decode($request->getContent(), true);
        $api_return = $submission_api->editFile($all, $submission_id, true);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $submission = $api_return['submission'];
                return new JsonResponse(
                    [
                        'success' => true,
                        'submission_id' => $submission->getId(),
                        'submission_slug' => $submission->getSlug()
                    ]
                );
                break;

            case 1:
                return new JsonResponse(
                    [
                        'success' => false,
                        'erorr' => 1
                    ],
                    404
                );
                break;
            case 2:
                return new JsonResponse(
                    [
                        'success' => false,
                        'erorr' => 1
                    ],
                    403
                );
                break;

            default:
                return new JsonResponse(
                    [
                        'success' => false,
                        'erorr' => $errcode,
                        'debug' => FormErrorFormatter::getErrorsAsArray($api_return['form'])
                    ]
                );
                break;
        }
    }
}
