<?php

namespace App\Controller\SubmissionManagement;

use App\Service\FormErrorFormatter;
use App\Service\SubmissionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserCreateSubmissionController extends Controller
{
    /**
     * @Route("/{_locale}/submission/create", name="user_create_file_submission", requirements={"_locale" = "en|fr"})
     * @param Request $request
     * @param SubmissionAPI $submission_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function submitFromSite(Request $request, SubmissionAPI $submission_api)
    {
        $api_return = $submission_api->submitNewFile($request->request->all());
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, submission successful
                $submission = $api_return['submission'];
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission->getId(),
                        'slug' => $submission->getSlug()
                    ]
                );
                break;

            case 1:
            case 2:
                // Form is not valid, or not submitted
                $form = $api_return['form'];
                return $this->render(
                    'submission_management/user_submit.html.twig',
                    [
                        'form' => $form->createView()
                    ]
                );
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/submission/create", name="user_submit_submission_api")
     * @Method("POST")
     * @param Request $request
     * @param SubmissionAPI $submission_api
     * @return JsonResponse
     */
    public function submitFromApi(Request $request, SubmissionAPI $submission_api)
    {
        $all = json_decode($request->getContent(), true);
        $api_return = $submission_api->submitNewFile($all, true);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $submission = $api_return['submission'];
                return new JsonResponse(
                    [
                        'success' => true,
                        'submission_id' => $submission->getId(),
                        'submission_slug' => $submission->getSlug()
                    ]
                );
                break;

            default:
                return new JsonResponse(
                    [
                        'success' => false,
                        'erorr' => $errcode,
                        'debug' => FormErrorFormatter::getErrorsAsArray($api_return['form'])
                    ]
                );
        }
    }
}
