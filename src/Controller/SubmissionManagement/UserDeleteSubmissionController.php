<?php

namespace App\Controller\SubmissionManagement;

use App\Service\SubmissionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserDeleteSubmissionController extends Controller
{
    /**
     * Only owners of the submission can delete their content. Moderators can only ban the content, with same effect.
     *
     * @Route("/{_locale}/submission/delete/{submission_id}/", name="user_delete_submission", requirements={"_locale" = "en|fr"})
     *
     * @param SubmissionAPI $submission_api
     * @param $submission_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function deleteFromSite(SubmissionAPI $submission_api, $submission_id)
    {
        $api_return = $submission_api->deleteFile($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, soft-deletion successful
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission_id,
                        // try to generate a slug beforehand if it's quick and easy. Here I'm way too lazy.
                    ]
                );

            case 1: // Submission not found
                throw $this->createNotFoundException();
            case 2: // Submission doesn't belong to the current user
            case 3: // Submission already banned or deleted
                throw $this->createAccessDeniedException();

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/submission/delete/{submission_id}", name="user_delete_submission_api")
     * @Method("POST")
     * @param SubmissionAPI $submission_api
     * @param $submission_id
     * @return JsonResponse|Response
     */
    public function deleteFromApi(SubmissionAPI $submission_api, $submission_id)
    {
        $api_return = $submission_api->deleteFile($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                // All went well, soft-deletion successful
                return new JsonResponse(['success' => true]);

            case 1: // Submission not found
                return new JsonResponse(['success' => false, 'error' => 1], 404);
            case 2: // Submission doesn't belong to the current user
            case 3: // Submission already banned or deleted
                return new JsonResponse(['success' => false, 'error' => 2], 403);


            default:
                return new JsonResponse(['success' => false, 'error' => 99], 500);
        }
    }
}
