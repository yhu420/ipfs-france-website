<?php

namespace App\Controller\Reactions;

use App\Service\FormErrorFormatter;
use App\Service\SubmissionAPI;
use App\Service\UserFileReactionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserUpvoteFileController extends Controller
{
    /**
     * @Route("/{_locale}/reaction/create/upvote/{submission_id}/", name="user_upvote_submission", requirements={"_locale" = "en|fr"})
     * @param $submission_id
     * @param UserFileReactionAPI $reaction_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function upvoteFromSite($submission_id, UserFileReactionAPI $reaction_api)
    {
        $api_return = $reaction_api->toggleUpvoteFromId($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $submission = $api_return['submission'];
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission->getId(),
                        'slug' => $submission->getSlug()
                    ]
                );
                break;

            case 1: // Submission not found
                throw $this->createNotFoundException('Submission not found');
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/reaction/create/upvote/{submission_id}", name="user_upvote_file_api")
     * @Method("POST")
     * @param $submission_id
     * @param UserFileReactionAPI $reaction_api
     * @return JsonResponse|Response
     */
    public function upvoteFromApi($submission_id, UserFileReactionAPI $reaction_api)
    {
        $api_return = $reaction_api->toggleUpvoteFromId($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return new JsonResponse([
                    'success' => true,
                    'vote' => $api_return['mod']
                ]);
                break;

            case 1: // Submission not found
                return new JsonResponse([
                    'success' => false,
                    'error' => 1
                ], 404);
                break;

            default:
                return new Response('What?', 500);
        }
    }
}
