<?php

namespace App\Controller\Reactions;

use App\Service\UserFileReactionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserReportFileController extends Controller
{
    /**
     * @Route("/{_locale}/reaction/create/report/{submission_id}/", name="user_report_submission", requirements={"_locale" = "en|fr"})
     * @param $submission_id
     * @param Request $request
     * @param UserFileReactionAPI $reaction_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function reportFromSite($submission_id, Request $request, UserFileReactionAPI $reaction_api)
    {
        $api_return = $reaction_api->toggleReportFromId($submission_id, $request->request->all());
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $submission = $api_return['submission'];
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission->getId(),
                        'slug' => $submission->getSlug()
                    ]
                );
                break;

            case 1: // Submission not found
                throw $this->createNotFoundException('Submission not found');
                break;

            case 2: // Malformed query
                throw new \Exception("Malformed query");
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/reaction/create/report/{submission_id}", name="user_report_file_api")
     * @Method("POST")=
     * @param Request $request
     * @param $submission_id
     * @param UserFileReactionAPI $reaction_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function reportFromApi(Request $request, $submission_id, UserFileReactionAPI $reaction_api)
    {
        $all = json_decode($request->getContent(), true);
        $api_return = $reaction_api->toggleReportFromId($submission_id, $all);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return new JsonResponse([
                    'success' => true,
                    'vote' => $api_return['mod']
                ]);
                break;

            case 1: // Submission not found
                return new JsonResponse([
                    'success' => false,
                    'error' => 1
                ], 404);
                break;

            case 2: // Malformed query
                return new JsonResponse([
                    'success' => false,
                    'error' => 2
                ], 200);
                break;

            default:
                return new Response('What?', 500);
        }
    }
}
