<?php

namespace App\Controller\Reactions;

use App\Service\FormErrorFormatter;
use App\Service\SubmissionAPI;
use App\Service\UserFileReactionAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserDownvoteFileController extends Controller
{
    /**
     * @Route("/{_locale}/reaction/create/downvote/{submission_id}/", name="user_downvote_submission", requirements={"_locale" = "en|fr"})
     * @param $submission_id
     * @param UserFileReactionAPI $reaction_api
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function downvoteFromSite($submission_id, UserFileReactionAPI $reaction_api)
    {
        $api_return = $reaction_api->toggleDownvoteFromId($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                $submission = $api_return['submission'];
                return $this->redirectToRoute('user_see_submission',
                    [
                        'submission_id' => $submission->getId(),
                        'slug' => $submission->getSlug()
                    ]
                );
                break;

            case 1: // Submission not found
                throw $this->createNotFoundException('Submission not found');
                break;

            default:
                return new Response('What?', 500);
        }
    }

    /**
     * @Route("/api/v1/reaction/create/downvote/{submission_id}", name="user_downvote_file_api")
     * @Method("POST")
     * @param $submission_id
     * @param UserFileReactionAPI $reaction_api
     * @return JsonResponse|Response
     */
    public function downvoteFromApi($submission_id, UserFileReactionAPI $reaction_api)
    {
        $api_return = $reaction_api->toggleDownvoteFromId($submission_id);
        $errcode = $api_return['errcode'];

        switch ($errcode) {
            case 0:
                return new JsonResponse([
                    'success' => true,
                    'vote' => $api_return['mod']
                ]);
                break;

            case 1: // Submission not found
                return new JsonResponse([
                    'success' => false,
                    'error' => 1
                ], 404);
                break;

            default:
                return new Response('What?', 500);
        }
    }
}
