<?php

namespace App\Controller\Login;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends Controller {

    /**
     * Clear the BEARER Cookie
     * @Route("/api/v1/logout", name="logout_api")
     */
    public function logout() {
        $response = $this->json(['success' => true]);
        $cookie = new Cookie(
            'BEARER',                 // cookie name, should be the same as in JWT settings
            null,                          // the cookie value, e.g. the generated JWT token
            new \DateTime('1970/01/01'),  // the expiration
            null,                       // the path
            null,                    // the domain, null means that Symfony will generate it on its own
            false,                     // secure, e.g. only via https
            true,                   // http only cookie, which is the default so no need to specify
            false,                      // raw
            null                    // the same-site parameter, can be 'lax' or 'strict'
        );
        $response->headers->setCookie($cookie);
        return $response;
    }
}