<?php

namespace App\Controller\Login;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class LoginController extends Controller
{
    /**
     * @Route("{_locale}/login", name="user_login", requirements={"_locale" = "en|fr"}, defaults={"_locale": "fr"})
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginFromForm(AuthenticationUtils $authenticationUtils)
    {
        return $this->render('login/login.html.twig', [
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * La route pour se deconnecter.
     * Mais celle ci ne doit jamais être executé car symfony l'interceptera avant.
     *
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        return;
    }

}
