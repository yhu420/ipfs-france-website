<?php

namespace App\Controller\Login;

use App\Service\UserAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class LoginWithGoogleController extends Controller
{
    /**
     * @Route("{_locale}/login/with-google", name="user_login_google", requirements={"_locale" = "en|fr"}, defaults={"_locale": "fr"})
     * @param Request $request
     * @param UserAPI $userAPI
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function loginFromForm(Request $request, UserAPI $userAPI)
    {
        $ar = $userAPI->sessionLoginGoogleToken($request, $request->request->get('google-jwt'));
        $e = $ar['errcode'];

        switch ($e) {
            case 0:
                return $this->redirectToRoute('go_to_main_page');
                break;

            default:
                return new Response('An error occurred (Code ' . $e . ')', 500);

        }
    }

    /**
     * @Route("/api/v1/login/with-google", name="user_login_google_api")
     * @Method("POST")
     * @param Request $request
     * @param UserAPI $userAPI
     * @return JsonResponse|Response
     */
    public function loginFromApi(Request $request, UserAPI $userAPI)
    {
        try {
            $all = json_decode($request->getContent(), true);
            $all = $all['google-jwt'];
        } catch (\Exception $e) {
            return new Response('Could not parse JSON or missing google-jwt key', 500);
        }
        $ar = $userAPI->JwtLoginGoogleToken($all);
        $e = $ar['errcode'];

        switch ($e) {
            case 0:
                $cookie = $ar['cookie'];
                $response = $this->json(['success' => true]);
                $response->headers->setCookie($cookie);
                return $response;

            default:
                return new JsonResponse([
                    'success' => false,
                    'error' => $e
                ]);

        }
    }
}