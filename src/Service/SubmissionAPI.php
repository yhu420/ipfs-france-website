<?php

namespace App\Service;

use App\Elasticsearch\FileSubmissionIndexer;
use App\Entity\FileSubmission;
use App\Entity\User;
use App\Form\FileSubmissionType;
use App\Form\UserReportFileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SubmissionAPI
{
    protected $forms;
    protected $em;
    protected $user;
    protected $elastic_indexer;
    protected $reac_api;
    protected $router;

    public function __construct(
        FormFactoryInterface $factory,
        EntityManagerInterface $orm,
        TokenStorageInterface $usr,
        FileSubmissionIndexer $eidx,
        UserFileReactionAPI $reactionAPI,
        RouterInterface $router
    )
    {
        $this->forms = $factory;
        $this->em = $orm;
        $this->user = $usr->getToken()->getUser();
        $this->elastic_indexer = $eidx;
        $this->reac_api = $reactionAPI;
        $this->router = $router;
    }

    private function elasticPersist(FileSubmission $submission) {
        $this->elastic_indexer->indexSubmission('submissions', $submission);
    }

    private function elasticDelete(FileSubmission $submission) {
        $this->elastic_indexer->deleteSubmission('submissions', $submission);
    }

    /**
     * @param array $all
     * @param bool $disable_csrf
     * @return array
     */
    public function submitNewFile(array $all, bool $disable_csrf = false) {
        $submission = new FileSubmission();
        $options = $disable_csrf ? ['csrf_protection' => false] : [];
        $form = $this->forms->create(FileSubmissionType::class, $submission, $options);

        if (!array_key_exists('file_submission', $all)) {
            // It's safe to assume the form hasn't been submitted
            return [
                'errcode' => 2,
                'form' => $form
            ];
        }

        $data = $all['file_submission'];
        //if (!isset($data['state'])) $data['state'] = 'default'; // Why did I do this? A constructor isn't okay?
        $form->submit($data);
        
        if ($form->isValid()) {

            $submission->setSubmitter($this->user);
            $this->reac_api->toggleUpvoteFromEntity($submission);

            $em = $this->em;
            $em->persist($submission);
            $em->flush();
            $this->elasticPersist($submission);

            return [
                'errcode' => 0,
                'submission' => $submission
            ];
        }

        return [
            'errcode' => 1,
            'form' => $form
        ];

        // ... do any other work - like sending them an email, etc
        // maybe set a "flash" success message for the user
    }

    /**
     * @param array $all
     * @param int $submission_id
     * @param bool $disable_csrf
     * @return array
     */
    public function editFile(array $all, int $submission_id, bool $disable_csrf = false) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);

        if (is_null($submission))
            return ['errcode' => 1];

        if (
            $submission->getSubmitter()->getId() != $this->user->getId()
            and
            !$this->user->hasRole('ROLE_ADMIN')
        )
            return ['errcode' => 2];

        $options = $disable_csrf ? ['csrf_protection' => false] : [];
        $form = $this->forms->create(FileSubmissionType::class, $submission, $options);
        if (!array_key_exists('file_submission', $all)) {
            // It's safe to assume the form hasn't been submitted
            return [
                'errcode' => 3,
                'form' => $form
            ];
        }

        $data = $all['file_submission'];
        //if (!isset($data['state'])) $data['state'] = $submission->getState(); // Same, does this have a purpose?
        $form->submit($data);

        if ($form->isValid()) {

            // We persist before elasticsearch. We want to make sure flush() worked before indexing
            // the new submission in the search engine
            $em = $this->em;
            $em->persist($submission);
            $em->flush();

            switch ($submission->getState()) {
                case 'deleted':
                    $submission->setModeratorsComments(
                        date(DATE_RFC2822) . ': User deleted his own submission'
                    );
                    $this->elasticDelete($submission);
                    break;
                case 'banned':
                    $submission->setModeratorsComments(
                        date(DATE_RFC2822) . ': ' . $this->user->getUsername() . ' banned this submission'
                    );
                    $this->elasticDelete($submission);
                    break;

                case 'verified':
                    $submission->setModeratorsComments(
                        date(DATE_RFC2822) . ': ' . $this->user->getUsername() . ' verified this submission'
                    );
                    $this->elasticPersist($submission);
                    break;
                default:
                    $this->elasticPersist($submission);
                    break;

            }

            // We flush again for the moderatorscomments, hoping this will work.
            $em->persist($submission);
            $em->flush();

            return [
                'errcode' => 0,
                'submission' => $submission
            ];
        }

        // Form isn't valid
        return [
            'errcode' => 4,
            'form' => $form
        ];
    }

    /**
     * @param int $submission_id
     * @return array
     */
    public function deleteFile(int $submission_id) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);
        if (is_null($submission))
            return ['errcode' => 1];

        if ($submission->getSubmitter()->getId() != $this->user->getId())
            return ['errcode' => 2];

        switch ($submission->getState()) {
            case 'deleted':
            case 'banned':
                return ['errcode' => 3];
        }

        $submission->setState('deleted');
        $submission->setModeratorsComments(date(DATE_RFC2822) . ': User deleted his own submission');
        $em = $this->em;
        $em->persist($submission);
        $em->flush();
        $this->elasticDelete($submission);

        return ['errcode' => 0];
    }

    /**
     * @param int $submission_id
     * @param $slug
     * @return array
     */
    public function seeSubmission(int $submission_id, $slug) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);
        $form = $this->forms->create(UserReportFileType::class, null,
            [
                'action' => $this->router->generate('user_report_submission', ['submission_id' => $submission_id]),
                'method' => 'POST'
            ]
        );

        if (is_null($submission))
            return ['errcode' => 1];


        switch ($submission->getState()) {
            case 'deleted':
            case 'banned':
            if ($this->user instanceof User) {
                if ($submission->getSubmitter()->getId() != $this->user->getId() // We are neither the owner
                    and
                    !$this->user->hasRole('ROLE_ADMIN') // Nor an administrator
                )
                    return ['errcode' => 2];

            }
        }

        $correct_slug = $submission->getSlug();
        if (!is_null($slug) && $correct_slug != $slug) {
            return ['errcode' => 3, 'correct_slug' => $correct_slug];
        }

        return [
            'errcode' => 0,
            'submission' => $submission,
            'report_form' => $form->createView()
        ];
    }
}