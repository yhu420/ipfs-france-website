<?php

namespace App\Service;

use App\Entity\FileSubmission;
use App\Entity\UserFileReaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserFileReactionAPI
{
    protected $em;
    protected $user;

    public function __construct(EntityManagerInterface $orm, TokenStorageInterface $usr)
    {
        $this->em = $orm;
        $this->user = $usr->getToken()->getUser();
    }

    public function toggleUpvoteFromId(int $submission_id) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);
        return $this->toggleUpvoteFromEntity($submission);
    }

    public function toggleUpvoteFromEntity(?FileSubmission $submission) {

        if (is_null($submission))
            return ['success' => false, 'errcode' => 1];

        // Whether the user wants to remove or add an upvote, there mustn't be any downvote
        if ($reaction = $this->user->getDownvote($submission)) {
            $this->em->remove($reaction);
            $this->updateCachedKarmaForReaction($reaction, true);
        }

        if ($reaction = $this->user->getUpvote($submission)) {
            $this->em->remove($reaction);
            $this->updateCachedKarmaForReaction($reaction, true);
            $mod = 'removal';
        } else {
            $reaction = $this->user->upvote($submission);
            $this->em->persist($reaction);
            $this->updateCachedKarmaForReaction($reaction, false);
            $mod = 'addition';
        }

        $this->em->flush();
        return ['success' => true, 'errcode' => 0, 'submission' => $submission, 'mod' => $mod];
    }

    public function toggleDownvoteFromId(int $submission_id) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);
        return $this->toggleDownvoteFromEntity($submission);
    }

    public function toggleDownvoteFromEntity(?FileSubmission $submission) {

        if (is_null($submission))
            return ['success' => false, 'errcode' => 1];

        // Whether the user wants to remove or add a downvote, there mustn't be any upvote
        if ($reaction = $this->user->getUpvote($submission)) {
            $this->em->remove($reaction);
            $this->updateCachedKarmaForReaction($reaction, true);
        }

        if ($reaction = $this->user->getDownvote($submission)) {
            $this->em->remove($reaction);
            $this->updateCachedKarmaForReaction($reaction, true);
            $mod = 'removal';
        } else {
            $reaction = $this->user->downvote($submission);
            $this->em->persist($reaction);
            $this->updateCachedKarmaForReaction($reaction, false);
            $mod = 'addition';
        }

        $this->em->flush();
        return ['success' => true, 'errcode' => 0, 'submission' => $submission, 'mod' => $mod];
    }

    public function toggleReportFromId(int $submission_id, ?array $data) {
        $submission = $this->em->getRepository(FileSubmission::class)->find($submission_id);
        return $this->toggleReportFromEntity($submission, $data);
    }

    public function toggleReportFromEntity(?FileSubmission $submission, ?array $data) {

        if (is_null($submission))
            return ['success' => false, 'errcode' => 1];

        if ($reaction = $this->user->getReport($submission)) {
            $this->em->remove($reaction);
            $mod = 'removal';
        } else {
            if (!is_null($data) &&
                array_key_exists('user_report_file', $data) &&
                array_key_exists('data', $data['user_report_file'])) {
                // It's safe to assume the form has been submitted
                // UserFileReaction is a value object, so no setters. I don't want to start making
                // data mappers, as there is only two properties, I better do this by hand.
                // Interesting reads:
                // * https://stackoverflow.com/questions/35965159/doctrine-without-setters
                // * https://webmozart.io/blog/2015/09/09/value-objects-in-symfony-forms/

                $data = $data['user_report_file'];
                $reason = $data['data'];
                $reason = substr($reason, 0, 255);
                $reaction = $this->user->reportFile($submission, $reason);
                $this->em->persist($reaction);
                $mod = 'addition';
            } else {
                return ['success' => false, 'errcode' => 2];
            }
        }

        $this->em->flush();
        return ['success' => true, 'errcode' => 0, 'submission' => $submission, 'mod' => $mod];
    }

    public function updateCachedKarmaForReaction(UserFileReaction $r, bool $removal)
    {
        $amount = $removal ? -1 : 1;
        $reaction = $r->getReaction();
        $f = $r->getFileSubmission();
        $o = $f->getSubmitter();
        switch ($reaction) {
            case 'upvote':
                $o->setKarma(intval($o->getKarma()) + $amount);
                $f->setKarma(intval($f->getKarma()) + $amount);
                break;
            case 'downvote':
                $o->setKarma(intval($o->getKarma()) - $amount);
                $f->setKarma(intval($f->getKarma()) - $amount);
                break;
        }

        $this->em->persist($o);
        $this->em->persist($f);
        $this->em->flush();
    }

}