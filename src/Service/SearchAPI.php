<?php

namespace App\Service;

use App\Form\AdvancedSearchType;
use Elastica\Client;
use Elastica\Query;
use Elastica\Query\BoolQuery;
use Elastica\Query\MultiMatch;
use Symfony\Component\Form\FormFactoryInterface;

class SearchAPI
{
    protected $ff;
    protected $client;

    public function __construct(FormFactoryInterface $ff, Client $client)
    {
        $this->client = $client;
        $this->ff = $ff;
    }


    public function getSearchResults(array $params) {
        if (isset($params['query']) && !empty($params['query']))
            $query = $params['query'];
        else
            return ['errcode' => 1];

        // TODO set page offset
        $page = isset($params['page']) && is_numeric($params['page']) ? $params['page'] : 1;
        $results_per_page = 10;

        $match = new MultiMatch();
        $match->setQuery($query);
        $match->setFields(["title^4", "summary", "description"]);

        $bool = new BoolQuery();
        $bool->addMust($match);

        if (isset($params['verifiedOnly']) && $params['verifiedOnly'] == 1) {
            $verifiedFilter = new Query\Term(['verified' => true]);
            $bool->addFilter($verifiedFilter);
        }

        if (isset($params['category']) && is_numeric($params['category'])) {
            $categoryFilter = new Query\Term(['category' => intval($params['category'])]);
            $parentCategoryFilter = new Query\Term(['parent_category' => intval($params['category'])]);
            $boolFilter = new BoolQuery();
            $boolFilter->addShould($categoryFilter)->addShould($parentCategoryFilter);
            $bool->addFilter($boolFilter);
        }

        $elasticaQuery = new Query($bool);
        $elasticaQuery->setSize($results_per_page);

/*        echo '<pre>';
        print_r($elasticaQuery->toArray());
        echo '</pre>';*/

        $foundPosts = $this->client->getIndex('submissions')->search($elasticaQuery);
        $results = [];
        foreach ($foundPosts as $post) {
            $results[] = $post->getSource();
        }

        return [
            'errcode' => 0,
            'results' => $results,
            'query' => $query,
            'form' => $this->ff->createNamed('',
                AdvancedSearchType::class,
                null,
                ['csrf_protection' => false]
            )->createView()
        ];
    }
}