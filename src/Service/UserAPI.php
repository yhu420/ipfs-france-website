<?php

namespace App\Service;

use App\Form\UserEditType;
use App\Form\UserRegistrationType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UserAPI
{
    protected $forms;
    protected $encoder;
    protected $em;
    protected $user;
    protected $token_storage;
    protected $session;
    protected $evt_disp;
    protected $jwt_man;

    public function __construct(
        FormFactoryInterface $factory,
        UserPasswordEncoderInterface $enc,
        EntityManagerInterface $orm,
        TokenStorageInterface $usr,
        SessionInterface $session,
        EventDispatcherInterface $evdisp,
        JWTTokenManagerInterface $jwt_man
    )
    {
        $this->forms = $factory;
        $this->encoder = $enc;
        $this->em = $orm;
        $this->user = $usr->getToken()->getUser();
        $this->token_storage = $usr;
        $this->session = $session;
        $this->evt_disp = $evdisp;
        $this->jwt_man = $jwt_man;
    }


    /**
     * @param array $all
     * @param bool $disable_csrf
     * @return array
     */
    public function registerNewUser(array $all, bool $disable_csrf = false) {
        $user = new User();
        $options = $disable_csrf ? ['csrf_protection' => false] : [];
        $form = $this->forms->create(UserRegistrationType::class, $user, $options);


        if (!array_key_exists('user_registration', $all)) {
            // It's safe to assume the form hasn't been submitted
            return [
                'errcode' => 2,
                'form' => $form
            ];
        }

        $data = $all['user_registration'];
        $form->submit($data);
        
        if ($form->isValid()) {

            $password = $this->encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->em;
            $em->persist($user);
            $em->flush();

            return [
                'errcode' => 0
            ];
        }

        return [
            'errcode' => 1,
            'form' => $form
        ];

        // ... do any other work - like sending them an email, etc
        // maybe set a "flash" success message for the user
    }

    /**
     * Check your .env for Google keys on failure
     *
     * @param array $all
     * @return array
     */
    public function registerNewUserGoogle(array $all) {

        $form = $this->forms->create(UserRegistrationType::class);
        if (!array_key_exists('username', $all)) {
            return ['errcode' => 1, 'form' => $form];
        }

        $username = $all['username'];
        if (strlen($username) < 2 || strlen($username) > 25) {
            return ['errcode' => 2, 'form' => $form];
        }

        if (!array_key_exists('google-jwt', $all)) {
            return ['errcode' => 3, 'form' => $form];
        }

        $client = new \Google_Client();
        $payload = $client->verifyIdToken($all['google-jwt']);
        if (!$payload) {
            return ['errcode' => 4, 'form' => $form];
        }

        $user = new User();
        $password = md5(uniqid()); // User will only be able to signin with Google
        $password = $this->encoder->encodePassword($user, $password);
        $user
            ->setUsername($username)
            ->setEmail($payload['email'])
            ->setPassword($password)
            ->setRegistrationPlatform('google')
        ;

        // See duplicate email / username
        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            return ['errcode' => 5, 'form' => $form];
        }

        return ['errcode' => 0];
    }

    /**
     * @param string $googleJWT
     * @return array|null|object
     */
    private function getUserForGoogleToken(?string $googleJWT) {
        $client = new \Google_Client();
        $payload = $client->verifyIdToken($googleJWT);
        if (!$payload) {
            return [
                'errcode' => 2
            ];
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $payload['email']]);
        if (is_null($user)) {
            return [
                'errcode' => 3
            ];
        }

        return $user;
    }

    /**
     * @param Request $request
     * @param string $googleJWT
     * @return array|null|object
     */
    public function sessionLoginGoogleToken(Request $request, $googleJWT) {
        try {
            $user = $this->getUserForGoogleToken($googleJWT);
        } catch (\Exception $e) {
            return ['errcode' => 1, 'debug' => $e->getMessage()];
        }
        if (is_array($user)) {
            return $user;
        }

        // Cf https://ourcodeworld.com/articles/read/459/how-to-authenticate-login-manually-an-user-in-a-controller-with-or-without-fosuserbundle-on-symfony-3

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->token_storage->setToken($token);
        $this->session->set('_security_main', serialize($token));
        $event = new InteractiveLoginEvent($request, $token);
        $this->evt_disp->dispatch('security.interactive_login', $event);

        return ['errcode' => 0];
    }

    /**
     * If you update the cookie, update AuthenticationSuccessListener as well
     *
     * @param string $googleJWT
     * @return array|null|object
     */
    public function JwtLoginGoogleToken($googleJWT) {
        try {
            $user = $this->getUserForGoogleToken($googleJWT);
        } catch (\Exception $e) {
            return ['errcode' => 1, 'debug' => $e->getMessage()];
        }
        if (is_array($user)) {
            return $user;
        }

        $token = $this->jwt_man->create($user);
        $cookie = new Cookie(
            'BEARER',                 // cookie name, should be the same as in JWT settings
            $token,                          // the cookie value, e.g. the generated JWT token
            new \DateTime('+1 hour'),  // the expiration
            null,                       // the path
            null,                    // the domain, null means that Symfony will generate it on its own
            false,                     // secure, e.g. only via https
            true,                   // http only cookie, which is the default so no need to specify
            false,                      // raw
            null                    // the same-site parameter, can be 'lax' or 'strict'
        );

        return [
            'errcode' => 0,
            'token' => $token,
            'cookie' => $cookie
        ];
    }

    /**
     * @param $username
     * @return array
     */
    public function seeUserProfile($username) {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);

        if (is_null($user))
            return ['errcode' => 1];

        return [
            'errcode' => 0,
            'user' => $user
        ];
    }

    /**
     * @return array
     */
    public function seeMyUserProfile() {
        $user = $this->user;

        if (is_null($user) || !($user instanceof User))
            return ['errcode' => 1, 'debug' => 'User is ' . $user];

        return [
            'errcode' => 0,
            'user' => $user
        ];
    }

    /**
     * @param array $all
     * @param string $username
     * @param bool $disable_csrf
     * @return array
     */
    public function editUserProfile(array $all, string $username, bool $disable_csrf = false) {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        $options = $disable_csrf ? ['csrf_protection' => false] : [];
        $form = $this->forms->create(UserEditType::class, $user, $options);

        if (is_null($user))
            return ['errcode' => 1];

        if ($user->getId() != $this->user->getId())
            return ['errcode' => 2];


        if (!array_key_exists('user_edit', $all)) {
            // It's safe to assume the form hasn't been submitted
            return [
                'errcode' => 3,
                'form' => $form->createView()
            ];
        }

        $data = $all['user_edit'];
        $form->submit($data);

        if ($form->isValid()) {

            $em = $this->em;
            $em->persist($user);
            $em->flush();

            return [
                'errcode' => 0,
                'form' => $form->createView()
            ];
        }

        return [
            'errcode' => 4,
            'form' => $form->createView()
        ];
    }

    /**
     * @param $assoc
     * @return array
     */
    public function editMyProfileAPI($assoc) {
        /** @var User $user */
        $user = $this->user;
        $em = $this->em;

        if (array_key_exists('bio', $assoc)) {
            $bio = substr($assoc['bio'], 0, 50);
            $user->setBio($bio);
            $em->persist($user);
        }

        if (array_key_exists('email', $assoc) && $email = filter_var($assoc['email'], FILTER_VALIDATE_EMAIL)) {
            $user->setEmail($email);
            $em->persist($user);
        }

        $em->flush();

        return ['errcode' => 0];
    }
}
