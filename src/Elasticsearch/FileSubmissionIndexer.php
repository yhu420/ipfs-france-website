<?php
namespace App\Elasticsearch;

use App\Entity\FileSubmission;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Client;
use Elastica\Document;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FileSubmissionIndexer
{
    private $client;
    private $submissionRepository;
    private $router;

    public function __construct(Client $client, EntityManagerInterface $em, UrlGeneratorInterface $router)
    {
        $this->client = $client;
        $this->submissionRepository = $em->getRepository(FileSubmission::class);
        $this->router = $router;
    }

    public function buildDocument(FileSubmission $post)
    {
        $category = $post->getCategory();
        while ($category->getParent() && $category->getParent()->getId() != $category->getId()) {
            $category = $category->getParent();
        }

        return new Document(
            $post->getId(), // Manually defined ID
            [
                'id' => $post->getId(),
                'title' => $post->getTitle(),
                'summary' => $post->getSummary(),
                'description' => $post->getDescription(),
                'category' => $post->getCategory()->getId(),
                'parent_category' => $category->getId(),
                'slug' => $post->getSlug(),
                'verified' => $post->getState() == 'verified'
            ],
            'file_submission' // Types are deprecated, to be removed in Elastic 7
        );
    }

    /**
     * WARNING: This is SLOW. Use only when CRUDing only ONE doument
     *
     * @param $indexname
     * @param $submission
     */
    public function indexSubmission($indexname, FileSubmission $submission)
    {
        $document = $this->buildDocument($submission);
        $index = $this->client->getIndex($indexname);
        try {
            $index->updateDocuments([$document]);
        } catch (\Exception $e) {
            $index->addDocuments([$document]);
        }
    }

    /**
     *
     * WARNING: This is SLOW. Use only when deleting only ONE doument
     *
     * @param $indexname
     * @param $submission
     */
    public function deleteSubmission($indexname, FileSubmission $submission)
    {
        $document = $this->buildDocument($submission);
        $index = $this->client->getIndex($indexname);
        $index->deleteDocuments([$document]);
    }

    public function indexAllDocuments($indexName)
    {
        $allPosts = $this->submissionRepository->findBy(['state' => ['default', 'verified']]);
        $index = $this->client->getIndex($indexName);

        $documents = [];
        foreach ($allPosts as $post) {
            $documents[] = $this->buildDocument($post);
        }

        $index->addDocuments($documents);
        $index->refresh();
    }
}