<?php
namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * In a perfect world, karma should balance as users vote. However, for performance reasons, IPFS-France tries to
 * avoid calculating the karma count on every request. Therefore, file submissions and users have karma, Reddit-style.
 * This karma is stored in the databased, and modified everytime a vote is created. But this also means there can
 * be bugs and errors, or nasty admis tweaking the karma count.
 * This command recalculates the karma for all the website. It might take some time, the code looks really bad, but
 * eventually this will work.
 *
 * Do NOT forget to write 'soft' after it, otherwise all the vote casts will be dropped, for a faster result.
 *
 * Class KarmaResetCommand
 * @package App\Command
 */
class KarmaResetCommand extends Command
{
    protected static $defaultName = 'karma:reset';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Makes karma perfectly balanced. As all things should be.')
            ->addArgument('soft', InputArgument::OPTIONAL, 'Input \'soft\' to keep the current votes');

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        // We need to drop those tables, because they have enums, and doctrine says it can't update them
        // So doctrine will just recreate them this way. Data WILL be lost.
        if ($input->getArgument('soft') != 'soft') {
            try {
                $this->em->getConnection()->query('DELETE FROM `user_file_reaction`;')->execute();
            } catch (\Exception $e) {
                $io->error('Could not delete vote casts.');
                die();
            }
        }

        // Recalculate users' karma
        try {
            $this->em->getConnection()->query('
            UPDATE
                `user` realu
            set karma = 
                (SELECT COUNT(*)
                FROM user_file_reaction as r
                WHERE r.reaction = "upvote"
                AND r.file_submission_id IN (SELECT fs.id FROM
                file_submission fs INNER JOIN (SELECT * FROM user) u
                ON u.id = fs.submitter_id
                WHERE u.id = realu.id))
                -
                (SELECT COUNT(*)
                FROM user_file_reaction as r
                WHERE r.reaction = "downvote"
                AND r.file_submission_id IN (SELECT fs.id FROM
                file_submission fs INNER JOIN (SELECT * FROM user) u
                ON u.id = fs.submitter_id
                WHERE u.id = realu.id))
            ')->execute();
        } catch (\Exception $e) {
            $io->error('Could not update profile karma');
            die();
        }

        try {
            $this->em->getConnection()->query('
            UPDATE
                file_submission realfs
            set karma = 
                (SELECT COUNT(*)
                FROM user_file_reaction as r LEFT JOIN (SELECT * FROM file_submission) as fs
                ON r.file_submission_id = fs.id
                WHERE r.reaction = "upvote"
                AND r.file_submission_id = realfs.id)
                -
                (SELECT COUNT(*)
                FROM user_file_reaction as r LEFT JOIN (SELECT * FROM file_submission) as fs
                ON r.file_submission_id = fs.id
                WHERE r.reaction = "downvote"
                AND r.file_submission_id = realfs.id);
            ')->execute();
        } catch (\Exception $e) {
            $io->error('Could not update file submissions karma');
            die();
        }

        $io->success('All done!');
    }
}