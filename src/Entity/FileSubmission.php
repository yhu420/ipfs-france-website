<?php

namespace App\Entity;

use App\Type\FileSubmissionState;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="hash", message="submission.multipleHash")
 * @UniqueEntity(fields="title", message="submission.multipleTitle")
 */
class FileSubmission
{

    public function __construct()
    {
        $this->setDateSubmitted(new \DateTime());
        $this->setState('default');
        $this->setKarma(0);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * =======================
     *  SIMPLE PROPERTIES
     * =======================
     */

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Assert\NotBlank(
     *     message = "submission.blankHash"
     * )
     */
    private $hash;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Assert\NotBlank(
     *     message = "submission.blankTitle"
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * The summary is a short description of the submitted file, that will show up for example in the search results
     */
    private $summary;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * The fully detailed description of the submitted file. Expect Markdown syntax.
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $dateSubmitted;

    /**
     * @ORM\Column(type="filesubmissionstateenum")
     */
    private $state;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * To be prefixed with moderator name and date
     */
    private $moderatorsComments;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $karma;

    /**
     * =======================
     * ORM RELATIONSHIPS
     * =======================
     */

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fileSubmissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $submitter;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="associatedFileSubmissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * ========================
     * METHODS
     * ========================
     */

    public function getSlug()
    {
        return preg_replace('#[^\\pL\d]+#u', '-', $this->title);
    }

    public function toArray(bool $return_author = true)
    {
        $r = [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'hash' => $this->getHash(),
            'summary' => $this->getSummary(),
            'description' => $this->getDescription(),
            'category' => $this->getCategory()->getId(),
            'state' => $this->getState(),
            'karma' => $this->getKarma(),
            'moderators_comments' => $this->getModeratorsComments(),
            'slug' => $this->getSlug(),
            'date_submitted' => $this->getDateSubmitted()->format('Y-m-d H:i:s')
        ];

        if ($return_author)
        {
            $r['author'] = [
                'id' => $this->getSubmitter()->getId(),
                'username' => $this->getSubmitter()->getUsername()
            ];
        }

        return $r;
    }

    /**
     * ========================
     * GETTERS / SETTERS
     * ========================
     */

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return FileSubmission
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return FileSubmission
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return FileSubmission
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param mixed $summary
     * @return FileSubmission
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return FileSubmission
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateSubmitted()
    {
        return $this->dateSubmitted;
    }

    /**
     * @param mixed $dateSubmitted
     * @return FileSubmission
     */
    public function setDateSubmitted($dateSubmitted)
    {
        $this->dateSubmitted = $dateSubmitted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubmitter()
    {
        return $this->submitter;
    }

    /**
     * @param mixed $submitter
     * @return FileSubmission
     */
    public function setSubmitter($submitter)
    {
        $this->submitter = $submitter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return FileSubmission
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return FileSubmission
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModeratorsComments()
    {
        return $this->moderatorsComments;
    }

    /**
     * @param mixed $moderatorsComments
     * @return FileSubmission
     */
    public function setModeratorsComments($moderatorsComments)
    {
        $this->moderatorsComments = $moderatorsComments;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKarma()
    {
        return $this->karma;
    }

    /**
     * @param mixed $karma
     * @return FileSubmission
     */
    public function setKarma($karma)
    {
        $this->karma = $karma;
        return $this;
    }
}