<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * About the multiple Ids: Every user can only submit one upvote or downvote, and one report
 * for one file submission. One user can't simultaneously upvote and downvote a file submission.
 * So in practice, we would not need 'reaction' to be in the constraint, but users can bot report and downvote
 * on the same submission. This will also prevent a user from sending multiple reports for the same submission.
 * This entity just serves the pupose of being an associative table and should never be accessed manually.
 * For example, if you want an user to upvote a post:
 * DONT:
 *  new UserFileReaction(user, file, 'upvote')
 * DO:
 *  $user->upvote(file);
 *
 * @ORM\Entity
 */
class UserFileReaction
{
    public function __construct(User $u, FileSubmission $f, string $reaction, string $data = null)
    {
        $this->submitter = $u;
        $this->fileSubmission = $f;
        $this->reaction = $reaction;
        $this->data = $data;
    }

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="FileSubmission")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fileSubmission;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $submitter;

    /**
     * @ORM\Id
     * @ORM\Column(type="userfilereactiontype")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reaction;

    /**
     * Would serve as the 'report' comment
     * @ORM\Column(type="string", nullable=true, length=255)
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "report.messageTooLong",
     * )
     */
    private $data;

    /**
     * === GETTERS ===
     */

    public function getReaction()
    {
        return $this->reaction;
    }

    public function getFileSubmission()
    {
        return $this->fileSubmission;
    }

    public function getSubmitter()
    {
        return $this->submitter;
    }

    public function getData()
    {
        return $this->data;
    }
}
