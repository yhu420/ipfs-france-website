<?php
// src/Entity/User.php
namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * 191 as maxlength is because  767 \ 4 = 191 (Max bytes in utf8 column \ bytes taken by a char = 191)
 *
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="register.multipleEmail")
 * @UniqueEntity(fields="username", message="register.multipleUsername")
 */
class User implements UserInterface
{
    public function __construct() {
        $this->roles = [ 'ROLE_USER' ];
        $this->registrationDate = new \DateTime();
        $this->karma = 0;
        $this->registrationPlatform = 'email';
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true, nullable=true)
     *
     * @Assert\Email()
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "register.emailTooLong",
     *      groups={"edit"}
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\NotBlank(
     *     message = "register.blankUsername"
     * )
     *
     * @Assert\Length(
     *      min = 2,
     *      max = 25,
     *      minMessage = "register.usernameTooShort",
     *      maxMessage = "register.usernameTooLong"
     * )
     */
    private $username;

    /**
     * @Assert\NotBlank(
     *     message = "register.blankPassword"
     * )
     *
     * @Assert\Length(
     *      min = 5,
     *      max = 25,
     *      minMessage = "register.passwordTooShort",
     *      maxMessage = "register.passwordTooLong"
     * )
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $registrationDate;

    /**
     * @ORM\Column(type="text", length=50, nullable=true)
     * @Assert\Length(
     *      max = 50,
     *      maxMessage = "user.bioTooLong",
     *      groups={"edit"}
     * )
     */
    private $bio;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private $karma;

    /**
     * @ORM\Column(type="registrationplatformtype", nullable=false)
     */
    private $registrationPlatform;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="FileSubmission", mappedBy="submitter")
     */
    private $fileSubmissions;

    /**
     * @ORM\OneToMany(targetEntity="UserFileReaction", mappedBy="submitter")
     */
    private $reactions;

    // other properties and methods

    public function toArray()
    {
        return [
            'username' => $this->getUsername(),
            'id' => $this->getId(),
            'registrationDate' => $this->getRegistrationDate()->format("Y-m-d H:i:s"),
            'bio' => $this->getBio(),
            'karma' => $this->getKarma(),
            'registrationPlatform' => $this->getRegistrationPlatform()
        ];
    }

    public function getFileSubmissions(): Collection
    {
        return $this->fileSubmissions;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function hasRole(string $role_name)
    {
        return in_array($role_name, $this->getRoles());
    }

    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    public function getKarma()
    {
        return $this->karma;
    }

    public function setKarma($karma)
    {
        $this->karma = $karma;
        return $this;
    }

    public function getRegistrationPlatform()
    {
        return $this->registrationPlatform;
    }

    public function setRegistrationPlatform($registrationPlatform)
    {
        $this->registrationPlatform = $registrationPlatform;
        return $this;
    }

    private function reactToFile($reaction, $file, $data = null)
    {
        return new UserFileReaction($this, $file, $reaction, $data);
    }

    public function upvote(FileSubmission $file)
    {
        return $this->reactToFile('upvote', $file);
    }

    public function downvote(FileSubmission $file)
    {
        return $this->reactToFile('downvote', $file);
    }

    public function reportFile(FileSubmission $file, string $comment)
    {
        return $this->reactToFile('report', $file, $comment);
    }

    public function getReactions()
    {
        return $this->reactions;
    }

    /**
     * @param Collection $c
     * @return null
     * @throws \Exception
     */
    private function getOnlyReactionOrNull(Collection $c)
    {
        if ($c->isEmpty())
            return null;

        if (count($c) != 1)
            throw new \Exception('Only one reaction expected, got ' . count($c));

        return $c->first();
    }

    public function getReactionsForFile(FileSubmission $submission) : Collection
    {
        return $this->getReactions()->filter(
            function ($entry) use ($submission) {
                return $entry->getFileSubmission()->getId() == $submission->getId();
            }
        );
    }

    public function getUpvote(FileSubmission $submission)
    {
        $r = $this->getReactionsForFile($submission)->filter(
            function ($entry) {
                return $entry->getReaction() == 'upvote';
            }
        );

        return $this->getOnlyReactionOrNull($r);
    }

    public function getDownvote(FileSubmission $submission)
    {
        $r = $this->getReactionsForFile($submission)->filter(
            function ($entry) {
                return $entry->getReaction() == 'downvote';
            }
        );

        return $this->getOnlyReactionOrNull($r);
    }

    public function getReport(FileSubmission $submission)
    {
        $r = $this->getReactionsForFile($submission)->filter(
            function ($entry) {
                return $entry->getReaction() == 'report';
            }
        );

        return $this->getOnlyReactionOrNull($r);
    }


    // Useless, but required by the interface
    public function eraseCredentials(){}
    public function getSalt(){return null;}
}
