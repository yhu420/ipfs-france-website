<?php

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\FileSubmission;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FileSubmissionsFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $first_user = $manager->getRepository(User::class)->findOneBy([]);
        $first_category = $manager->getRepository(Category::class)->findOneBy([]);

        if (is_null($first_user) or is_null($first_category)) {
            throw new \Exception(
                "
                You must have at least one user and one Category in your database.
                Make sure to use --append when loading fixtures too.
                "
            );
        }

        for ($i = 0; $i < 50; $i++) {
            $s = new FileSubmission();
            $s->setTitle($this->getRandomWordsAsStrig(3));
            $s->setSummary($this->getRandomWordsAsStrig(7));
            $s->setDescription($this->getRandomWordsAsStrig(20));
            $s->setHash(md5(microtime()));
            $s->setState('default');
            $s->setSubmitter($first_user);
            $s->setCategory($first_category);
            $manager->persist($s);
        }
        $manager->flush();
    }

    private function getRandomWordsAsStrig($count) {
        $text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum eget interdum leo, a semper sapien.
         Ut eu semper velit. Morbi aliquam lorem nec arcu molestie, in hendrerit magna sollicitudin. Aenean nec nisl eros.
         Quisque a viverra urna. Donec eleifend urna ut purus dictum rutrum. Ut eu turpis et nulla malesuada bibendum.
         Alright now I\'ve figured there are sometimes duplicates so integrity constraints fail. Therefore I add text to
         avoid getting more of these.';
        $text = preg_replace('/\s{2,}/', ' ', $text); // Remove multiple whitespaces, but keep \t and \n
        $words = explode(' ', $text);

        $result = [];
        for ($i = 0; $i < $count; $i++) {
            $result[] = $words[rand(0, count($words)-1)];
        }
        $result = implode(' ', $result);

        return $result;
    }
}