<?php
namespace App\Type;

class UserFileReactionType extends EnumType
{
    protected $name = 'userfilereactiontype';
    protected $values = ['upvote', 'downvote', 'report'];

    public static function getReadableAvailableValues() {
        return [
            'Upvote' => 'upvote',
            'Downvote' => 'downvote',
            'Report' => 'report'
        ];
    }
}