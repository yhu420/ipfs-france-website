<?php
namespace App\Type;

class FileSubmissionState extends EnumType
{
    protected $name = 'filesubmissionstateenum';
    protected $values = ['default', 'banned', 'deleted', 'verified'];

    public static function getReadableAvailableValues() {
        return [
            'Default' => 'default',
            'Banned' => 'banned',
            'Deleted' => 'deleted',
            'Verified' => 'verified'
        ];
    }
}