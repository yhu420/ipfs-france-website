<?php
namespace App\Type;

class RegistrationPlatformType extends EnumType
{
    protected $name = 'registrationplatformtype';
    protected $values = ['email', 'google'];

    public static function getReadableAvailableValues() {
        return [
            'Email' => 'email',
            'Google' => 'google'
        ];
    }
}