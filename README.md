IPFS website REST API
===

This repository contains all the source code and data that makes IPFS-France.
Although this website is available in french, the codebase is written in english.

### What is IPFS-France?

This website aims to make it easy for people to share IPFS links, and search for others.
This way, you can share files efficiently without having to fear for them to go 404 or taken down.

### What are the main features?

* Basic administration
* ElasticSearch search engine
* Simple REST API with JWT authentication

### How do I install this?

Make sure you have elasticsearch installed! ($ sudo pacman -S elasticsearch (for fellow Manjaro users))

    $ git clone <repo>
    $ cd ipfs-france
    $ composer install
    $ composer update
    $ ./bin/start
    
If you want to use the API, you might want to have a look at [the official lexik doc](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md).
For those who are offline here is the doc on how to generate the SSH keys:

Generate the SSH keys :

    $ mkdir -p config/jwt # For Symfony3+, no need of the -p option
    $ openssl genrsa -out config/jwt/private.pem -aes256 4096
    $ openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem

In case first openssl command forces you to input password use following to get the private key decrypted
(It worked for me when doing this between the two 'openssl' commands)

    $ openssl rsa -in config/jwt/private.pem -out config/jwt/private2.pem
    $ mv config/jwt/private.pem config/jwt/private.pem-back
    $ mv config/jwt/private2.pem config/jwt/private.pem

WARNING: Elasticsearch will not work if php is ran in a PHP-FPM conatainer. (because localhost refers to the containers). TODO: find a fix

Coding conventions
===

## Route naming convention

Please drop everything that is not VueJS related regarding the frontend

The CRUD terms are replaced with

* Create
* Read : no authentication required
* Request : authentiation required
* Edit : Shorter than update
* Delete 

## ElasticSearch

For the shell scripts to work in ./bin, you have to be able to start the elasticsearch server by running:

    $ sudo systemctl start elasticsearch

Leave it with the default options, listening on 127.0.0.1:9200.
Error handling is poor, so make sure to make pull requests if you run into trouble!

There is also a command to reindex all (non banned) posts:

    $ ./bin/console elastic:reindex
    
    
## Promoting users

For now, only two roles are in use. You can alternate between ROLE_USER and ROLE_ADMIN with the command:

    $ bin/console user:swrole <username>
    
## Updating the database

Just `d:s:u -f` for now
    
## Loading fixtures

Make sure you have at least one user and one category in your database.
Then run the command

    $ ./bin/console doctrine:fixtures:load --append
    $ ./bin/console elastic:reindex
    
## Recalculating the karma

Karma relative to every entity is saved in the database in an attenmpt to prevent having to recalculate
it for every request. This means the karma can sometimes be off, because the update failed or any
other reason.

Run the command `bin/console karma:reset soft` to recalculate all the karma.
Remove the `soft` option and all the vote casts will be deleted
